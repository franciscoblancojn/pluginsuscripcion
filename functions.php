<?php
function PS10_is_suscription()
{
    $user_id = get_current_user_id();
    if($user_id == 0) return false;

    $order_statuses = array('wc-processing', 'wc-completed');
    $orders = wc_get_orders( array(
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_status' => $order_statuses,
        'numberposts' => -1,
        'returns' => 'ids',
    ) );
    // foreach ($orders as $key => $order) {
    //     $order_id = $order->ID;
    //     $dateOrder = $order->get_date_created();
    // }
    if(count($orders)==0)return false;

    $order_id = $orders[0]->ID;
    $dateOrder = $orders[0]->get_date_created();

    // echo $dateOrder;
    // echo "<br>";
    //add months
    $dateOrder = date('c', strtotime("+1 months", strtotime($dateOrder)));

    $SdateOrder = strtotime($dateOrder);
    
    $toDay = date("c");
    $StoDay = strtotime($toDay);
    
    // echo $dateOrder;
    // echo "<br>";
    // echo $toDay;
    if($StoDay < $SdateOrder){
        // echo "yes";
    }

    return $StoDay < $SdateOrder;
}