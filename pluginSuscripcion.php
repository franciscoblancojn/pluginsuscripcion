<?php
/*
Plugin Name: Plugin Suscripcion
Plugin URI: https://startscoinc.com/es/
Description: Plugin para detectar la compra de una suscripcion para wordpress
Author: Startsco
Version: 0.1
Author URI: https://startscoinc.com/es/#
License: 
*/
/**
 * Require admin
 * Is field for requiere fields
 */
define("prefix_PS10","PS10");
define("PS10_location",plugin_dir_path( __FILE__ ));

require_once plugin_dir_path( __FILE__ ) . 'functions.php';
require_once plugin_dir_path( __FILE__ ) . 'shortcode.php';
require_once plugin_dir_path( __FILE__ ) . 'widget/admin.php';