<?php
function shortcodeValidateSuscripcion( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'url'  =>  '/pagar/?add-to-cart=443'
    ), $atts );

    if(!PS10_is_suscription()){
        $url = $settings["url"];
        ?>
        <script>
            window.location = "<?=esc_attr($a['url'])?>"
        </script>
        <?php
        exit;
    }
}
add_shortcode( 'shortcodeValidateSuscripcion', 'shortcodeValidateSuscripcion' );