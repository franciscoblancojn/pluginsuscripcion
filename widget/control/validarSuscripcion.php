<?php

$this->start_controls_section(
    'content_config',
    [
        'label'     => __( 'Config', prefix_PS10 ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'active',
    [
        'label'         => __( 'Activo', prefix_PS10 ),
        'type'          => \Elementor\Controls_Manager::SWITCHER,
        'description'   => __( 'Si desea activar la validacion de suscripcion seleccione esta opcion, y agrege este widget al inicio de la pagina', prefix_PS10 ),
    ]
);
$this->add_control(
    'url',
    [
        'label'         => __( 'Url', prefix_PS10 ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'description'   => __( 'En caso de que el usuario no cumpla con suscripcion a que URL desea redireccionarlo', prefix_PS10 ),
    ]
);
$this->end_controls_section();