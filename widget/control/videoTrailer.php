<?php

$this->start_controls_section(
    'content_trailer',
    [
        'label'     => __( 'Trailer', prefix_PS10 ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'trailer',
    [
        'label'         => __( 'Trailer', prefix_PS10 ),
        'type'          => \Elementor\Controls_Manager::TEXTAREA,
        'description'   => __( 'Iframe del Trailer en Vimeo', prefix_PS10 ),
        'rows'          => 10,
    ]
);
$this->end_controls_section();

$this->start_controls_section(
    'content_btn',
    [
        'label'     => __( 'Boton', prefix_PS10 ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$this->add_control(
    'btn',
    [
        'label'         => __( 'Texto', prefix_PS10 ),
        'type'          => \Elementor\Controls_Manager::TEXT,
        'description'   => __( 'Texto del Boton Suscripcion', prefix_PS10 ),
        'default'       => __('Suscribete para ver todos los videos',prefix_PS10)
    ]
);
$this->end_controls_section();
$this->start_controls_section(
    'content_product',
    [
        'label'     => __( 'Producto', prefix_PS10 ),
        'tab'       => \Elementor\Controls_Manager::TAB_CONTENT,
    ]
);
$all_ids = get_posts( array(
    'post_type' => 'product',
    'numberposts' => -1,
    'post_status' => 'publish',
    'fields' => 'ids',
) );
$options = [];
foreach ( $all_ids as $id ) {
    $product = wc_get_product( $id );
    $options[$id] = $product->get_name();
}
$this->add_control(
    'product',
    [
        'label'         => __( 'Producto', prefix_PS10 ),
        'type'          => \Elementor\Controls_Manager::SELECT,
        'description'   => __( 'Seleccione el Producto de suscripcion', prefix_PS10 ),
        'options' => $options,
    ]
);
$this->end_controls_section();