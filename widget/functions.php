<?php
function PS10_add_control_style($name = "", $css = "", $this_ , $config = array(), $title ="")
{
    $this_->start_controls_section(
            $name,
            [
                'label' => __( $title ),
                'tab' => \Elementor\Controls_Manager::TAB_STYLE,
            ]
        );
        if(isset($config['hover']) && $config['hover']){
        $this_->start_controls_tabs(
            'style_tabs_'.$name
        );
            $this_->start_controls_tab(
                'style_normal_tab_'.$name,
                [
                    'label' => __( 'Normal' ),
                ]
            );
            }
                if(isset($config['background']) && $config['background']){
                    $this_->add_group_control(
                        \Elementor\Group_Control_Background::get_type(),
                        [
                            'name' => 'background_'.$name,
                            'label' => __( 'Background' ),
                            'types' => [ 'classic', 'gradient'],
                            'selector' => '{{WRAPPER}} '.$css,
                        ]
                    );
                }
                if(isset($config['align']) && $config['align']){
                    $this_->add_control(
                       'align_'.$name,
                        [
                            'label' => __( 'Alignment' ),
                            'type' => \Elementor\Controls_Manager::CHOOSE,
                            'options' => [
                                'left' => [
                                    'title' => __( 'Left' ),
                                    'icon' => 'fa fa-align-left',
                                ],
                                'center' => [
                                    'title' => __( 'Center' ),
                                    'icon' => 'fa fa-align-center',
                                ],
                                'right' => [
                                    'title' => __( 'Right' ),
                                    'icon' => 'fa fa-align-right',
                                ]
                            ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css => 'text-align: {{VALUE}};',
                            ],
                            'default' => 'center',
                        ]
                    );
                }
                
                if(isset($config['typography']) && $config['typography']){
                    $this_->add_group_control(
                        \Elementor\Group_Control_Typography::get_type(),
                        [
                            'name' => 'input_typography_'.$name,
                            'label' => __( 'Typography' ),
                            'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                            'selector' => '{{WRAPPER}} '.$css,
                        ]
                    );
                }
                if(isset($config['color']) && $config['color']){
                    $this_->add_control(
                        'color_'.$name,
                        [
                            'label' => __( 'Color', 'plugin-domain' ),
                            'type' => \Elementor\Controls_Manager::COLOR,
                            'scheme' => [
                                'type' => \Elementor\Scheme_Color::get_type(),
                                'value' => \Elementor\Scheme_Color::COLOR_1,
                            ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css => 'color: {{VALUE}}',
                            ],
                        ]
                    );
                }
                if(isset($config['border']) && $config['border']){
                    $this_->add_group_control(
                        \Elementor\Group_Control_Border::get_type(),
                        [
                            'name' => 'border_'.$name,
                            'selector' => '{{WRAPPER}} '.$css,
                        ]
                    );
                }
                if(isset($config['borderRadius']) && $config['borderRadius']){
                    $this_->add_control(
                        'borderRadius_'.$name,
                        [
                            'label' => __('Border Radius'),
                            'type' => \Elementor\Controls_Manager::DIMENSIONS,
                            'size_units' => [ 'px', '%', 'em' ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                            ],
                        ]
                    );
                }
                if(isset($config['padding']) && $config['padding']){
                    $this_->add_control(
                        'padding_'.$name,
                        [
                            'label' => __( 'Padding'),
                            'type' => \Elementor\Controls_Manager::DIMENSIONS,
                            'size_units' => [ 'px', '%', 'em' ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                            ],
                        ]
                    );
                }
                if(isset($config['margin']) && $config['margin']){
                    $this_->add_control(
                        'margin_'.$name,
                        [
                            'label' => __( 'Margin' ),
                            'type' => \Elementor\Controls_Manager::DIMENSIONS,
                            'size_units' => [ 'px', '%', 'em' ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                            ],
                        ]
                    );
                }
        if(isset($config['hover']) && $config['hover']){
            $this_->end_controls_tab();
            $this_->start_controls_tab(
                'style_hover_tab_'.$name,
                [
                    'label' => __( 'Hover' ),
                ]
            );
                if(isset($config['background']) && $config['background']){
                    $this_->add_group_control(
                        \Elementor\Group_Control_Background::get_type(),
                        [
                            'name' => 'background_hover_'.$name,
                            'label' => __( 'Background' ),
                            'types' => [ 'classic', 'gradient' ],
                            'selector' => '{{WRAPPER}} '.$css.":hover",
                        ]
                    );
                }
                if(isset($config['typography']) && $config['typography']){
                    $this_->add_group_control(
                        \Elementor\Group_Control_Typography::get_type(),
                        [
                            'name' => 'input_typography_hover_'.$name,
                            'label' => __( 'Typography', 'plugin-domain' ),
                            'scheme' =>  \Elementor\Scheme_Typography::TYPOGRAPHY_1,
                            'selector' => '{{WRAPPER}} '.$css.":hover",
                        ]
                    );
                }
                if(isset($config['color']) && $config['color']){
                    $this_->add_control(
                        'color_hover_'.$name,
                        [
                            'label' => __( 'Color', 'plugin-domain' ),
                            'type' => \Elementor\Controls_Manager::COLOR,
                            'scheme' => [
                                'type' => \Elementor\Scheme_Color::get_type(),
                                'value' => \Elementor\Scheme_Color::COLOR_1,
                            ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css.":hover" => 'color: {{VALUE}}',
                            ],
                        ]
                    );
                }
                if(isset($config['border']) && $config['border']){
                    $this_->add_group_control(
                        \Elementor\Group_Control_Border::get_type(),
                        [
                            'name' => 'border_hover_'.$name,
                            'selector' => '{{WRAPPER}} '.$css.":hover",
                        ]
                    );
                }
                if(isset($config['padding']) && $config['padding']){
                    $this_->add_control(
                        'padding_hover_'.$name,
                        [
                            'label' => __( 'Padding'),
                            'type' => \Elementor\Controls_Manager::DIMENSIONS,
                            'size_units' => [ 'px', '%', 'em' ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css.":hover" => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                            ],
                        ]
                    );
                }
                if(isset($config['margin']) && $config['margin']){
                    $this_->add_control(
                        'margin_hover_'.$name,
                        [
                            'label' => __( 'Margin' ),
                            'type' => \Elementor\Controls_Manager::DIMENSIONS,
                            'size_units' => [ 'px', '%', 'em' ],
                            'selectors' => [
                                '{{WRAPPER}} '.$css.":hover" => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                            ],
                        ]
                    );
                }
            $this_->end_controls_tab();
        $this_->end_controls_tabs();
        }
    $this_->end_controls_section();
}