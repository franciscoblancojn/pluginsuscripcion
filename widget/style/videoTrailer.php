<?php
PS10_add_control_style( "content_video" , ".content_video" , $this , array(
    'padding' => true,
    'border' => true,
    'background' => true,
    'margin' => true,
) , 'Content Video');
PS10_add_control_style( "Iframe" , "iframe" , $this , array(
    'padding' => true,
    'border' => true,
    'background' => true,
    'margin' => true,
) , 'Iframe');
PS10_add_control_style( "content_button" , ".content_btn" , $this , array(
    'padding' => true,
    'border' => true,
    'background' => true,
    'margin' => true,
    'align' => true,
) , 'Content Button');
PS10_add_control_style( "button" , "a" , $this , array(
    'hover' => true,
    'padding' => true,
    'border' => true,
    'color' => true,
    'background' => true,
    'typography' => true,
    'margin' => true,
    'borderRadius'=>true,
) , 'Button');