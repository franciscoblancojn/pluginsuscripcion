<?php
if(!(\Elementor\Plugin::$instance->editor->is_edit_mode())){
    $active = $settings["active"];
    if($active == "yes"){
        if(!PS10_is_suscription()){
            $url = $settings["url"];
            ?>
            <script>
                window.location = "<?=$url?>"
            </script>
            <?php
            exit;
        }
    }
}