<?php
/**
 * Widget for Elmentor Cuestionario
 */
function PS10_validarSuscripcion_f()
{
    class PS10_validarSuscripcion extends \Elementor\Widget_Base {

        /**
         * Get widget name.
         *
         * Retrieve oEmbed widget name.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget name.
         */
        public function get_name() {
            return 'Validar Suscripcion';
        }
    
        /**
         * Get widget title.
         *
         * Retrieve oEmbed widget title.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget title.
         */
        public function get_title() {
            return __( 'Validar Suscripcion' );
        }
    
        /**
         * Get widget icon.
         *
         * Retrieve oEmbed widget icon.
         *
         * @since 1.0.0
         * @access public
         *
         * @return string Widget icon.
         */
        public function get_icon() {
            return 'eicon-lock-user';
        }
    
        /**
         * Get widget categories.
         *
         * Retrieve the list of categories the oEmbed widget belongs to.
         *
         * @since 1.0.0
         * @access public
         *
         * @return array Widget categories.
         */
        public function get_categories() {
            return [ 'general' ];
        }
    
        /**
         * Register oEmbed widget controls.
         *
         * Adds different input fields to allow the user to change and customize the widget settings.
         *
         * @since 1.0.0
         * @access protected
         */
        protected function _register_controls() {
            require_once plugin_dir_path( __FILE__ ).'control/validarSuscripcion.php';
        }
    
        /**
         * Render oEmbed widget output on the frontend.
         *
         * Written in PHP and used to generate the final HTML.
         *
         * @since 1.0.0
         * @access protected
         */
        protected function render() {
            $settings = $this->get_settings_for_display();
            require_once plugin_dir_path( __FILE__ ).'template/validarSuscripcion.php';
        }
    
    }
}
add_action( 'elementor_pro/init', function() {
    PS10_validarSuscripcion_f();
    \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \PS10_validarSuscripcion() );
});
